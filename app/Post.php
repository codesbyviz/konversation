<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function User()
    {
        return $this->belongsTo("App\User","user","id");
    }
    public function Category()
    {
        return $this->belongsTo("App\Category","category","id");
    }
    public function light()
    {
        return substr($this->content, 0, 200);
    }
}
