require('./bootstrap');
window.Vue = require('vue');
window.Noty = require('noty');
window.randomColor = require('randomcolor');
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

import VueRouter from 'vue-router';
import routes from './routes';
import store from './store';
import NProgress from 'nprogress';
import 'vue-loaders/dist/vue-loaders.css';
import * as VueLoaders from 'vue-loaders';
import VueSimplemde from 'vue-simplemde'
import 'simplemde/dist/simplemde.min.css'
import VoerroTagsInput from '@voerro/vue-tagsinput';
import '@voerro/vue-tagsinput/dist/style.css';

// NProgress Config

NProgress.configure({ 
    minimum: 0.1,
    easing: 'ease',
    speed: 500,
    showSpinner: false
});
Noty.overrideDefaults({
    layout   : 'topRight',
    theme    : 'sunset',
    timeout:"4000"
});
const router = new VueRouter({
    routes: routes,
    linkExactActiveClass:"active",
    mode: 'history'
});

Vue.use(VueLoaders);
Vue.use(VueRouter);
Vue.use(VueSimplemde)
Vue.use(require('vue-moment'));

Vue.component('tags-input', VoerroTagsInput);

Vue.component('main-component', require('./components/Master/Main.vue').default);

const app = new Vue({
    el: '#app',
    router: router,
    store:store,
});
router.beforeEach((to, from, next) => {
    NProgress.start();
    store.getters.Auth;
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.state.token) {
          next({
            path: '/login',
            query: { redirect: to.fullPath }
          })
        } else {
          next()
        }
      } 
      else if (to.matched.some(record => record.meta.guest)) {
        if (store.state.token) {
            next({path: '/home'});
        }
        else{
            next();
        }
      }     
      else {
        next()
      }
});
router.afterEach((to, from) => {
    NProgress.done();
})
